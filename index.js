const dotenv = require('dotenv')

dotenv.config()

console.log('Starting feedless web')

require('./src/lib/errors')

require('./src/lib/ssb')

let server
setTimeout(() => {
  server = require('./src/lib/express')
}, 500)

if (process.env.NODE_ENV != 'production') {
  const chokidar = require('chokidar')
  const watcher = chokidar.watch('./src/lib')

  watcher.on('ready', () => {
    watcher.on('all', () => {
      console.log('Clearing /lib/ module cache from server')
      Object.keys(require.cache).forEach(id => {
        if (id.includes('metrics')) return
        if (/[\/\\]lib[\/\\]/.test(id)) delete require.cache[id]
      })
      if (server && server.close) server.close()
      server = require('./src/lib/express')
    })
  })
}
